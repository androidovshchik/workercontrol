package com.workercontrol.components;

import android.content.Context;
import android.location.GpsStatus.Listener;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.helper.ItemTouchHelper.Callback;

import java.util.ArrayList;
import java.util.List;

public class LocationControl implements LocationListener, Listener {

    public static final int UPDATE_PERIOD_MILLIS = 10000;
    private static final int TWO_MINUTES = 120000;
    @SuppressWarnings("all")
    public ArrayList<String> mActiveProviders = new ArrayList();
    private Context mContext;
    private boolean mGpsEnabled = false;
    private Location mLastLocation;
    private LocationManager mLocationManager;

    @SuppressWarnings("all")
    public LocationControl(Context context) {
        this.mContext = context;
        this.mLocationManager = (LocationManager) context.getSystemService("location");
        if (ContextCompat.checkSelfPermission(context, "android.permission.ACCESS_FINE_LOCATION") == 0 ||
            ContextCompat.checkSelfPermission(context, "android.permission.ACCESS_COARSE_LOCATION") == 0) {
            List<String> providers = this.mLocationManager.getProviders(false);
            if (providers.contains("gps")) {
                useProvider("gps");
            }
            if (providers.contains("network")) {
                useProvider("network");
            }
        }
    }

    @SuppressWarnings("all")
    public Location getLastLocation() {
        Location gpsLocation = null;
        Location netLocation = null;
        if (this.mActiveProviders.contains("gps")) {
            gpsLocation = this.mLocationManager.getLastKnownLocation("gps");
        }
        if (this.mActiveProviders.contains("network")) {
            netLocation = this.mLocationManager.getLastKnownLocation("network");
        }
        if (netLocation == null) {
            mLastLocation = gpsLocation;
        } else if (isBetterLocation(netLocation, gpsLocation)) {
            mLastLocation = netLocation;
        } else {
            mLastLocation = gpsLocation;
        }
        return mLastLocation;
    }

    public boolean isGpsEnabled() {
        return this.mGpsEnabled;
    }

    public void onDestroy() {
        if (ContextCompat.checkSelfPermission(this.mContext, "android.permission.ACCESS_FINE_LOCATION") == 0 ||
            ContextCompat.checkSelfPermission(this.mContext, "android.permission.ACCESS_COARSE_LOCATION") == 0) {
            this.mLocationManager.removeUpdates(this);
        }
    }

    public void onLocationChanged(Location location) {
        if (location == null) {
            return;
        }
        if (isBetterLocation(location, this.mLastLocation)) {
            this.mLastLocation = location;
        }
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
        if (ContextCompat.checkSelfPermission(this.mContext, "android.permission.ACCESS_FINE_LOCATION") != 0 &&
            ContextCompat.checkSelfPermission(this.mContext, "android.permission.ACCESS_COARSE_LOCATION") != 0) {
            return;
        }
        if (status == 2) {
            useProvider(provider);
        } else {
            this.mActiveProviders.remove(provider);
        }
    }

    @SuppressWarnings("all")
    private void useProvider(String provider) {
        if (provider == "gps") {
            this.mGpsEnabled = true;
            this.mLocationManager.addGpsStatusListener(this);
            this.mLocationManager.requestLocationUpdates("gps", 10000, 0.0f, this);
            this.mActiveProviders.add("gps");
        }
        if (provider == "network") {
            this.mLocationManager.requestLocationUpdates("network", 10000, 0.0f, this);
            this.mActiveProviders.add("network");
        }
    }

    public void onProviderEnabled(String provider) {
        useProvider(provider);
    }

    public void onProviderDisabled(String provider) {
        this.mActiveProviders.remove(provider);
    }

    public int countActiveProviders() {
        return this.mActiveProviders.size();
    }

    private boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            return true;
        }
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > 120000;
        boolean isSignificantlyOlder = timeDelta < -120000;
        boolean isNewer = timeDelta > 0;
        if (isSignificantlyNewer) {
            return true;
        }
        if (isSignificantlyOlder) {
            return false;
        }
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > Callback.DEFAULT_DRAG_ANIMATION_DURATION;
        boolean isFromSameProvider = isSameProvider(location.getProvider(), currentBestLocation.getProvider());
        return isMoreAccurate || isNewer && !isLessAccurate ||
            isNewer && !isSignificantlyLessAccurate && isFromSameProvider;
    }

    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        } else {
            return provider1.equals(provider2);
        }
    }

    public void onGpsStatusChanged(int event) {
        boolean z = true;
        if (event != 1) {
            z = false;
        }
        this.mGpsEnabled = z;
    }
}
