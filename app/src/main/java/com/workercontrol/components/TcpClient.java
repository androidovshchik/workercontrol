package com.workercontrol.components;

import android.os.SystemClock;
import android.util.Log;

import com.workercontrol.ITcpClient;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.Date;

import javax.net.SocketFactory;

public class TcpClient implements ITcpClient {

    public static final String BEGIN_TAG = "$";
    public static final String DELIMITTER = ",";
    public static final String END_TAG = "!";
    public static final int MAX_PORT = 65535;
    public static final int MIN_PORT = 0;
    public static final String STOP_TAG = "*";
    private static final int TIMEOUT = 60000;
    private int failedSending = 0;
    private Object lock = new Object();
    private BufferedReader mReader;
    private Socket mSocket;
    private BufferedWriter mStreamWriter;

    public boolean openConnection(String host, int port) {
        try {
            SocketAddress sAddress = new InetSocketAddress(InetAddress.getByName(host), port);
            this.mSocket = SocketFactory.getDefault().createSocket();
            this.mSocket.setKeepAlive(true);
            this.mSocket.connect(sAddress, 60000);
            this.mStreamWriter = new BufferedWriter(new OutputStreamWriter(this.mSocket.getOutputStream()));
            this.lock = this.mStreamWriter;
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public String getHost() {
        return this.mSocket != null ? this.mSocket.getRemoteSocketAddress().toString() : "";
    }

    public int getPort() {
        return this.mSocket != null ? this.mSocket.getPort() : -1;
    }

    private void write(String location) throws IOException {
        if (this.mSocket.isConnected()) {
            if (location != null && location.length() > 0) {
                this.mStreamWriter.write(location);
                this.mStreamWriter.flush();
            }
            Log.d("WorkerControl", new Date().toString());
            Log.d("WorkerControl", "Sent");
            Log.d("WorkerControl", location);
        }
    }

    public void sendData(String data) throws IOException {
        synchronized (this.lock) {
            try {
                write(data);
                this.failedSending = 0;
            } catch (IOException e) {
                this.failedSending++;
                if (this.failedSending == 1) {
                    throw e;
                }
            }
        }
    }

    public String getAnswer() throws IOException {
        String answer;
        long targetTime = SystemClock.elapsedRealtime() + 30000;
        do {
            answer = this.mReader.readLine();
            if (SystemClock.elapsedRealtime() >= targetTime) {
                break;
            }
        } while (answer == null);
        return answer;
    }

    public void sleep(long time) {
        synchronized (this.lock) {
            try {
                this.lock.wait(time);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean isConnected() {
        if (this.mSocket != null) {
            return this.mSocket.isConnected();
        }
        return false;
    }

    public void closeConnection() throws IOException {
        if (this.mSocket.isConnected()) {
            this.mStreamWriter.close();
        }
    }
}
