package com.workercontrol.components;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AlertDialog;

import com.workercontrol.R;

public class Utils {

    public static final String NOTIFICATION_DEFAULT_CHANNEL_ID = "default_channel";

    @SuppressWarnings("all")
    public static Notification makeNotification(Context context, String message, int icon, PendingIntent click, boolean isImmutable) {
        return new NotificationCompat.Builder(context)
            .setSmallIcon(icon)
            .setContentTitle(context.getResources().getString(R.string.app_name))
            .setContentText(message)
            .setContentIntent(click)
            .setAutoCancel(!isImmutable)
            .setChannelId(NOTIFICATION_DEFAULT_CHANNEL_ID)
            .build();
    }

    @SuppressWarnings("all")
    public static void hideNotification(Context context, int notificationId) {
        ((NotificationManager) context.getSystemService("notification")).cancel(notificationId);
    }

    @SuppressWarnings("all")
    public static AlertDialog createDialog(Context ctx, String message, boolean isCancelable) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setIcon(R.mipmap.ic_launcher);
        builder.setTitle(ctx.getResources().getString(R.string.app_name));
        builder.setMessage((CharSequence) message);
        builder.setCancelable(isCancelable);
        return builder.create();
    }

    @SuppressWarnings("all")
    public static boolean isRunning(Context context, Class<? extends Service> serviceClass) {
        ActivityManager manager = (ActivityManager)
            context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static void startServiceRightWay(Context context, Class<? extends Service> serviceClass) {
        if (!isRunning(context, serviceClass)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(new Intent(context, serviceClass));
            } else {
                context.startService(new Intent(context, serviceClass));
            }
        }
    }

    @SuppressWarnings("all")
    public static void stopServiceRightWay(Context context, Class<? extends Service> serviceClass) {
        if (isRunning(context, serviceClass)) {
            context.stopService(new Intent(context, serviceClass));
        }
    }
}
