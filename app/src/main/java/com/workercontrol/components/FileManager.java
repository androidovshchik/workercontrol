package com.workercontrol.components;

import android.content.Context;
import android.text.format.DateFormat;

import com.snatik.storage.Storage;
import com.workercontrol.BuildConfig;

import java.io.File;

public class FileManager {

    @SuppressWarnings("all")
    public static void debug(Context context, Class<?> clss, String... messages) {
        if (!BuildConfig.DEBUG) {
            return;
        }
        Storage storage = new Storage(context);
        String path = storage.getExternalStorageDirectory() + File.separator + "workercontrol.debug.txt";
        StringBuilder message = new StringBuilder();
        message.append(DateFormat.format("yyyy-MM-dd hh:mm:ss", System.currentTimeMillis()));
        message.append(" --- STARTING_DEBUG_LOG ---\n");
        message.append("From class: " + clss.getSimpleName() + "\n");
        for (int m = 0; m < messages.length; m++) {
            message.append(messages[m] + "\n");
        }
        try {
            if (storage.isFileExist(path)) {
                storage.appendFile(path, message.toString());
            } else {
                storage.createFile(path, message.toString());
            }
        } catch (Exception e) {}
    }

    @SuppressWarnings("all")
    public static void exception(Context context, Class<?> clss, String... messages) {
        Storage storage = new Storage(context);
        String path = storage.getExternalStorageDirectory() + File.separator + "workercontrol.error.txt";
        StringBuilder message = new StringBuilder();
        message.append(DateFormat.format("yyyy-MM-dd hh:mm:ss", System.currentTimeMillis()));
        message.append(" --- STARTING_ERROR_LOG ---\n");
        message.append("From class: " + clss.getSimpleName() + "\n");
        for (int m = 0; m < messages.length; m++) {
            message.append(messages[m] + "\n");
        }
        try {
            if (storage.isFileExist(path)) {
                storage.appendFile(path, message.toString());
            } else {
                storage.createFile(path, message.toString());
            }
        } catch (Exception e) {}
    }
}
