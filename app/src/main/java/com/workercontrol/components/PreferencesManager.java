package com.workercontrol.components;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.workercontrol.models.Preferences;

public class PreferencesManager {

    public static Preferences readPreferences(SharedPreferences preferences) {
        Preferences result = new Preferences();
        if (preferences.contains(Preferences.HOST_FIELD)) {
            result.setHost(preferences.getString(Preferences.HOST_FIELD, Preferences.DEFAULT_HOST));
        }
        if (preferences.contains(Preferences.PORT_FIELD)) {
            result.setPort(preferences.getInt(Preferences.PORT_FIELD, Preferences.DEFAULT_PORT));
        }
        if (preferences.contains(Preferences.USERNAME_FIELD)) {
            result.setUserName(preferences.getLong(Preferences.USERNAME_FIELD, 0));
        }
        if (preferences.contains(Preferences.PERIOD_FIELD)) {
            result.setPeriod(preferences.getLong(Preferences.PERIOD_FIELD, 120));
        }
        if (preferences.contains(Preferences.IS_TRACKING_FIELD)) {
            result.setTracking(preferences.getBoolean(Preferences.IS_TRACKING_FIELD, false));
        }
        return result;
    }

    public static void writePreferences(Preferences item, SharedPreferences preferences) {
        Editor editor = preferences.edit();
        editor.putString(Preferences.HOST_FIELD, item.getHost());
        editor.putInt(Preferences.PORT_FIELD, item.getPort());
        editor.putLong(Preferences.USERNAME_FIELD, item.getUserName());
        editor.putBoolean(Preferences.IS_TRACKING_FIELD, item.isTracking());
        editor.putLong(Preferences.PERIOD_FIELD, item.getPeriod());
        editor.apply();
    }
}
