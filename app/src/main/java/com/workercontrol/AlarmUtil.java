package com.workercontrol;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.SystemClock;

import com.workercontrol.components.FileManager;
import com.workercontrol.services.ServiceTrigger;

public class AlarmUtil {

    @SuppressWarnings("all")
    public static void next(Context context, long interval, Class clss) {
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0,
            new Intent(context, ServiceTrigger.class), 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime() + interval, pendingIntent);
        } if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            alarmManager.setExact(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime() + interval, pendingIntent);
        } else {
            alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime() + interval, pendingIntent);
        }
        FileManager.debug(context, AlarmUtil.class, "New alarm with delay in " + interval +
            " milliseconds from class " + clss.getSimpleName());
    }
}
