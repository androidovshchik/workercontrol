package com.workercontrol;

import android.Manifest;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.AsyncTask;

import com.workercontrol.components.FileManager;

import java.lang.Thread.UncaughtExceptionHandler;

public class WorkerControlApplication extends Application {

    public void onCreate() {
        super.onCreate();
        StethoTool.init(getApplicationContext());
        Thread.setDefaultUncaughtExceptionHandler(new C02581());
    }

    private void CacheException(Throwable e) {
        FileManager.exception(getApplicationContext(), WorkerControlApplication.class, e.toString());
    }

    @SuppressWarnings("all")
    public static void getDeviceSuperInfo(final Context context) {
        if (!BuildConfig.DEBUG) {
            return;
        }
        if (context.getPackageManager().checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE,
            context.getPackageName()) == PackageManager.PERMISSION_GRANTED) {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    String s = "Debug-info:", error = null;
                    try {
                        s += "\n OS API Level: "    + android.os.Build.VERSION.SDK_INT;
                        s += "\n Device: "          + android.os.Build.DEVICE;
                        s += "\n Model (and Product): " + android.os.Build.MODEL
                            + " ("+ android.os.Build.PRODUCT + ")";
                        s += "\n RELEASE: "         + android.os.Build.VERSION.RELEASE;
                        s += "\n BRAND: "           + android.os.Build.BRAND;
                        s += "\n CPU_ABI: "         + android.os.Build.CPU_ABI;
                        s += "\n CPU_ABI2: "        + android.os.Build.CPU_ABI2;
                        s += "\n MANUFACTURER: "    + android.os.Build.MANUFACTURER;
                    } catch (Exception e) {
                        error = e.getMessage().toString();
                    }
                    FileManager.debug(context, WorkerControlApplication.class, "Device",
                        "Info > " + s, "Error > " + error);
                    return null;
                }
            }.execute();
        }
    }

    class C02581 implements UncaughtExceptionHandler {
        C02581() {
        }

        public void uncaughtException(Thread thread, Throwable ex) {
            WorkerControlApplication.this.CacheException(ex);
        }
    }
}
