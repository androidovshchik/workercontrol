package com.workercontrol;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.workercontrol.components.FileManager;
import com.workercontrol.components.PreferencesManager;
import com.workercontrol.components.TcpClient;
import com.workercontrol.components.Utils;
import com.workercontrol.models.Preferences;
import com.workercontrol.services.MainService;

import java.io.IOException;
import java.util.Hashtable;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static String PREF_NAME = "LoggingServicePref";
    private TextView isRunning;
    private TextInputLayout mHost;
    private TextInputLayout mPeriod;
    private TextInputLayout mPort;
    private Preferences mPref = new Preferences();
    private TextInputLayout mUserName;
    private Hashtable<Integer, String> messages;

    @SuppressWarnings("all")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SharedPreferences sharedPref = getSharedPreferences(PREF_NAME, 0);
        if (sharedPref.getAll().size() > 0) {
            this.mPref = PreferencesManager.readPreferences(sharedPref);
        }
        this.messages = loadMessages();
        initWidgets();
        Dexter.withActivity(this)
            .withPermissions(new String[] {
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            })
            .withListener(new MultiplePermissionsListener() {
                @Override
                public void onPermissionsChecked(MultiplePermissionsReport report) {
                    WorkerControlApplication.getDeviceSuperInfo(getApplicationContext());
                }
                @Override
                public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions,
                                                               PermissionToken token) {
                    saveLogs("Permissions denied");
                }
            })
            .check();
        //mPref.setTracking(true);
        //AlarmUtil.next(getApplicationContext(), mPref.getPeriod() * 1000L, MainActivity.class);
        //Utils.startServiceRightWay(getApplicationContext(), MainService.class);
    }

    @SuppressWarnings("all")
    private void saveLogs(final String... messages) {
        if (!BuildConfig.DEBUG) {
            return;
        }
        if (getPackageManager().checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE,
            getPackageName()) == PackageManager.PERMISSION_GRANTED) {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    FileManager.debug(getApplicationContext(), MainActivity.class, messages);
                    return null;
                }
            }.execute();
        }
    }

    @SuppressWarnings("all")
    private Hashtable<Integer, String> loadMessages() {
        Hashtable<Integer, String> result = new Hashtable();
        result.put(Integer.valueOf(R.string.field_required), getResources().getString(R.string.field_required));
        result.put(Integer.valueOf(R.string.invalid_port), getResources().getString(R.string.invalid_port));
        result.put(Integer.valueOf(R.string.invalid_id), getResources().getString(R.string.invalid_id));
        result.put(Integer.valueOf(R.string.invalid_number), getResources().getString(R.string.invalid_number));
        result.put(Integer.valueOf(R.string.unavailable_host), getResources().getString(R.string.unavailable_host));
        result.put(Integer.valueOf(R.string.service_active), getResources().getString(R.string.service_active));
        result.put(Integer.valueOf(R.string.service_inactive), getResources().getString(R.string.service_inactive));
        return result;
    }

    @SuppressWarnings("all")
    private void initWidgets() {
        this.mHost = (TextInputLayout) findViewById(R.id.host);
        this.mPort = (TextInputLayout) findViewById(R.id.port);
        this.mUserName = (TextInputLayout) findViewById(R.id.userName);
        this.mPeriod = (TextInputLayout) findViewById(R.id.period);
        this.isRunning = (TextView) findViewById(R.id.isRunning);
        final FloatingActionButton mServiceControl = (FloatingActionButton) findViewById(R.id.service_control);
        ((TextView) findViewById(R.id.version)).setText("Версия " + BuildConfig.VERSION_NAME + " " +
            (BuildConfig.DEBUG ? "DEBUG" : "RELEASE"));
        if (this.mPref != null) {
            this.mHost.getEditText().setText(this.mPref.getHost());
            this.mPort.getEditText().setText(Integer.toString(this.mPref.getPort()));
            this.mUserName.getEditText().setText(Long.toString(this.mPref.getUserName()));
            this.mPeriod.getEditText().setText(Long.toString(this.mPref.getPeriod()));
            enableInput(!this.mPref.isTracking());
            changeFabState(mServiceControl, this.mPref.isTracking());
        }
        mServiceControl.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                if (!MainActivity.this.isConnectionActive()) {
                    saveLogs("!this.isConnectionActive()", "Showing dialog");
                    AlertDialog dialog = Utils.createDialog(MainActivity.this, MainActivity.this.getResources().getString(R.string.client_disconnected_message), true);
                    dialog.setButton(-1, MainActivity.this.getResources().getText(R.string.open_preferences), new C02541());
                    dialog.show();
                } else if (MainActivity.this.mPref.isTracking()) {
                    saveLogs("this.mPref.isTracking()", "Stopping service");
                    Utils.stopServiceRightWay(MainActivity.this.getApplicationContext(), MainService.class);
                    MainActivity.this.mPref.setTracking(false);
                    setPrefs();
                    MainActivity.this.enableInput(true);
                    MainActivity.this.changeFabState(mServiceControl, false);
                } else {
                    boolean hasErrors = false;
                    if (MainActivity.this.mHost.getEditText().getText().length() == 0) {
                        hasErrors = true;
                        MainActivity.this.mHost.setError((CharSequence) MainActivity.this.messages.get(Integer.valueOf(R.string.field_required)));
                    }
                    if (MainActivity.this.mPort.getEditText().getText().length() == 0) {
                        MainActivity.this.mPort.setError((CharSequence) MainActivity.this.messages.get(Integer.valueOf(R.string.field_required)));
                        hasErrors = true;
                    } else if (Integer.parseInt(MainActivity.this.mPort.getEditText().getText().toString()) > 65535 || Integer.parseInt(MainActivity.this.mPort.getEditText().getText().toString()) < 0) {
                        MainActivity.this.mPort.setError((CharSequence) MainActivity.this.messages.get(Integer.valueOf(R.string.invalid_port)));
                        hasErrors = true;
                    }
                    if (MainActivity.this.mUserName.getEditText().getText().length() == 0) {
                        hasErrors = true;
                        MainActivity.this.mUserName.setError((CharSequence) MainActivity.this.messages.get(Integer.valueOf(R.string.field_required)));
                    } else if (MainActivity.this.mUserName.getEditText().getText().length() != 10) {
                        hasErrors = true;
                        MainActivity.this.mUserName.setError((CharSequence) MainActivity.this.messages.get(Integer.valueOf(R.string.invalid_id)));
                    }
                    if (MainActivity.this.mPeriod.getEditText().getText().length() == 0) {
                        hasErrors = true;
                        MainActivity.this.mPeriod.setError((CharSequence) MainActivity.this.messages.get(Integer.valueOf(R.string.field_required)));
                    }
                    saveLogs("Else on fab click", "hasErrors " + hasErrors,
                        MainActivity.this.mPref.toString());
                    if (hasErrors) {
                        MainActivity.this.changeFabState(mServiceControl, false);
                        return;
                    }
                    setPrefs();
                    new CheckHost(mServiceControl, MainActivity.this.mHost).execute(new Void[0]);
                }
            }

            class C02541 implements DialogInterface.OnClickListener {
                C02541() {
                }

                public void onClick(DialogInterface dialog, int which) {
                    MainActivity.this.startActivity(new Intent("android.settings.WIRELESS_SETTINGS"));
                }
            }
        });
    }

    @SuppressWarnings("all")
    private boolean isConnectionActive() {
        return ((ConnectivityManager) getSystemService("connectivity")).getActiveNetworkInfo().isConnected();
    }

    private boolean canConnectToHost(String host, int port) {
        ITcpClient mClient = new TcpClient();
        boolean result = mClient.openConnection(host, port);
        if (result) {
            try {
                mClient.closeConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    @SuppressWarnings("all")
    private void enableInput(boolean enabled) {
        this.mHost.setEnabled(enabled);
        this.mPort.setEnabled(enabled);
        this.mUserName.setEnabled(enabled);
        this.mPeriod.setEnabled(enabled);
        if (enabled) {
            this.isRunning.setText((CharSequence) this.messages.get(Integer.valueOf(R.string.service_inactive)));
        } else {
            this.isRunning.setText((CharSequence) this.messages.get(Integer.valueOf(R.string.service_active)));
        }
    }

    private void changeFabState(FloatingActionButton button, boolean isServiceActive) {
        if (isServiceActive) {
            button.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorServiceActive)));
            button.setImageResource(R.drawable.ic_pause);
            return;
        }
        button.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorServiceInactive)));
        button.setImageResource(R.drawable.ic_play);
    }

    @SuppressWarnings("all")
    private void setPrefs() {
        if (this.mHost.getEditText().getText().length() > 0) {
            this.mPref.setHost(this.mHost.getEditText().getText().toString());
        }
        if (this.mPort.getEditText().getText().length() > 0) {
            try {
                int port = Integer.parseInt(this.mPort.getEditText().getText().toString());
                if (port >= 0 && port <= 65535) {
                    this.mPref.setPort(port);
                }
            } catch (IllegalArgumentException e) {
                this.mPort.setError((CharSequence) this.messages.get(Integer.valueOf(R.string.invalid_number)));
            }
        }
        if (this.mUserName.getEditText().getText().length() == 10) {
            try {
                this.mPref.setUserName(Long.parseLong(this.mUserName.getEditText().getText().toString()));
            } catch (IllegalArgumentException e2) {
                e2.printStackTrace();
                this.mUserName.setError((CharSequence) this.messages.get(Integer.valueOf(R.string.invalid_number)));
            }
        }
        if (this.mPeriod.getEditText().getText().length() > 0) {
            try {
                this.mPref.setPeriod(Long.parseLong(this.mPeriod.getEditText().getText().toString()));
            } catch (IllegalArgumentException e22) {
                e22.printStackTrace();
                this.mPeriod.setError((CharSequence) this.messages.get(Integer.valueOf(R.string.invalid_number)));
            }
        }
        PreferencesManager.writePreferences(this.mPref, getSharedPreferences(PREF_NAME, 0));
    }

    protected void onDestroy() {
        super.onDestroy();
        setPrefs();
    }

    @SuppressWarnings("all")
    private class CheckHost extends AsyncTask<Void, Void, Boolean> {

        private TextInputLayout mHostField;
        private FloatingActionButton mRunning;

        public CheckHost(FloatingActionButton isRunning, TextInputLayout hostField) {
            mRunning = isRunning;
            mHostField = hostField;
        }

        protected Boolean doInBackground(Void... params) {
            return Boolean.valueOf(MainActivity.this.canConnectToHost(mPref.getHost(), mPref.getPort()));
        }

        protected void onPostExecute(Boolean canConnect) {
            saveLogs("onPostExecute", "canConnect: " + canConnect.booleanValue());
            if (canConnect.booleanValue()) {
                mHostField.setError(null);
                enableInput(false);
                changeFabState(mRunning, true);
                mPref.setTracking(true);
                setPrefs();
                AlarmUtil.next(getApplicationContext(), mPref.getPeriod() * 1000L, MainActivity.class);
                Utils.startServiceRightWay(getApplicationContext(), MainService.class);
                return;
            }
            mHostField.setError((CharSequence) messages.get(Integer.valueOf(R.string.unavailable_host)));
            changeFabState(mRunning, false);
        }
    }
}
