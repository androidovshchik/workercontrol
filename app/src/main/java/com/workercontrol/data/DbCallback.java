package com.workercontrol.data;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.db.SupportSQLiteOpenHelper;

public class DbCallback extends SupportSQLiteOpenHelper.Callback {

    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "backup.sqlite";

    private static final String CREATE_BACKUP_TABLE = "CREATE TABLE backup (" +
        "    timestamp INTEGER NOT NULL," +
        "    data      TEXT    NOT NULL," +
        "    sent      BOOLEAN NOT NULL DEFAULT (0)" +
        ");";

    public DbCallback() {
        super(DATABASE_VERSION);
    }

    @Override
    public void onCreate(SupportSQLiteDatabase db) {
        db.execSQL(CREATE_BACKUP_TABLE);
    }

    @Override
    public void onUpgrade(SupportSQLiteDatabase db, int oldVersion, int newVersion) {}
}