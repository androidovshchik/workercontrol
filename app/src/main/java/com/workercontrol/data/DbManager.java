package com.workercontrol.data;

import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.db.SupportSQLiteOpenHelper.Configuration;
import android.arch.persistence.db.SupportSQLiteOpenHelper.Factory;
import android.arch.persistence.db.framework.FrameworkSQLiteOpenHelperFactory;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.squareup.sqlbrite3.BriteDatabase;
import com.squareup.sqlbrite3.SqlBrite;
import com.workercontrol.BuildConfig;
import com.workercontrol.models.Row;

import io.reactivex.schedulers.Schedulers;

public class DbManager {

    public BriteDatabase db;

    public DbManager(Context context) {
        Configuration configuration = Configuration.builder(context)
            .name(DbCallback.DATABASE_NAME)
            .callback(new DbCallback())
            .build();
        Factory factory = new FrameworkSQLiteOpenHelperFactory();
        SupportSQLiteOpenHelper openHelper = factory.create(configuration);
        db = new SqlBrite.Builder()
            .logger(new SqlBrite.Logger() {
                @Override
                public void log(String message) {
                    Log.v(getClass().getSimpleName(), message);
                }
            })
            .build()
            .wrapDatabaseHelper(openHelper, Schedulers.io());
        db.setLoggingEnabled(BuildConfig.DEBUG);
    }

    @SuppressWarnings("all")
    public long insertRow(Row row) {
        return db.insert(row.getTable(), SQLiteDatabase.CONFLICT_REPLACE, row.toContentValues());
    }

    @SuppressWarnings("all")
    public int updateRow(Row row) {
        return db.update(row.getTable(), SQLiteDatabase.CONFLICT_IGNORE, row.toContentValues(),
            "rowid=?", String.valueOf(row.rowId));
    }

    @SuppressWarnings("all")
    public int deleteRow(Row row) {
        return db.delete(row.getTable(), "rowid=?", String.valueOf(row.rowId));
    }

    @SuppressWarnings("all")
    public int deleteTable(String table) {
        return db.delete(table, null, null);
    }
}