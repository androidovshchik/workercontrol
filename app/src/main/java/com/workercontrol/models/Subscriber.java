package com.workercontrol.models;

import android.util.Log;

import io.reactivex.observers.DisposableObserver;

public abstract class Subscriber<T> extends DisposableObserver<T> {

    @Override
    public void onComplete() {}

    @Override
    public void onError(Throwable e) {
        Log.e(getClass().getSimpleName(), e.toString());
    }
}