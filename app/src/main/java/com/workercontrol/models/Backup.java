package com.workercontrol.models;

import android.content.ContentValues;
import android.database.Cursor;

public class Backup extends Row {

	private static final String COLUMN_TIMESTAMP = "timestamp";
	private static final String COLUMN_DATA = "data";
	private static final String COLUMN_SENT = "sent";

	public long timestamp;

	public String data;

	public boolean sent;

	public Backup() {}

	public Backup(WorkerLocation workerLocation) {
		this.timestamp = workerLocation.getTimeInSeconds();
		this.data = workerLocation.toString();
		this.sent = false;
	}

	@Override
	public ContentValues toContentValues() {
		ContentValues values = new ContentValues();
		if (rowId != NONE) {
			values.put(COLUMN_ROW_ID, rowId);
		}
		values.put(COLUMN_TIMESTAMP, timestamp);
		values.put(COLUMN_DATA, data);
		values.put(COLUMN_SENT, sent ? 1 : 0);
		return values;
	}

	@Override
	public void parseCursor(Cursor cursor) {
		rowId = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_ROW_ID));
		timestamp = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_TIMESTAMP));
		data = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_DATA));
		sent = cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_SENT)) == 1;
	}

	@Override
	public String getTable() {
		return "backup";
	}

	@Override
	public String toString() {
		return "Backup{" +
			"rowId=" + rowId +
			", timestamp=" + timestamp +
			", data='" + data + '\'' +
			", sent=" + sent +
			'}';
	}
}