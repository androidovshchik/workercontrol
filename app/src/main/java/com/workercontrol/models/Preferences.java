package com.workercontrol.models;

public class Preferences {

    public static final String DEFAULT_HOST = "localhost";
    public static final int DEFAULT_PERIOD = 120;
    public static final int DEFAULT_PORT = 5000;
    public static final String HOST_FIELD = "Host";
    public static final String IS_TRACKING_FIELD = "IsTracking";
    public static final String PERIOD_FIELD = "Period";
    public static final String PORT_FIELD = "Port";
    public static final String USERNAME_FIELD = "UserName";
    private String mHost = DEFAULT_HOST;
    private boolean mIsTracking = false;
    private long mPeriod = 120;
    private int mPort = DEFAULT_PORT;
    private long mUserName = 0;

    public String getHost() {
        return this.mHost;
    }

    public void setHost(String host) {
        this.mHost = host;
    }

    public int getPort() {
        return this.mPort;
    }

    public void setPort(int port) {
        this.mPort = port;
    }

    public long getUserName() {
        return this.mUserName;
    }

    public void setUserName(long userId) {
        this.mUserName = userId;
    }

    public long getPeriod() {
        return this.mPeriod;
    }

    public void setPeriod(long period) {
        this.mPeriod = period;
    }

    public boolean isTracking() {
        return this.mIsTracking;
    }

    public void setTracking(boolean isServiceTracking) {
        this.mIsTracking = isServiceTracking;
    }

    @Override
    public String toString() {
        return "Preferences{" +
            "mHost='" + mHost + '\'' +
            ", mIsTracking=" + mIsTracking +
            ", mPeriod=" + mPeriod +
            ", mPort=" + mPort +
            ", mUserName=" + mUserName +
            '}';
    }
}
