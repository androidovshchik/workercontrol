package com.workercontrol.models;

import android.location.Location;

import java.util.Date;

public class WorkerLocation {

    public static final int USER_ID_LENGTH = 10;
    private long mBlockNum = 0;
    private float mDepth = 0.0f;
    private double mDirection = 0.0d;
    private int mGpsStatus = 0;
    private double mLatitude = 0.0d;
    private double mLongitude = 0.0d;
    private int mProtocolVersion = 1;
    private float mSpeed = 0.0f;
    private int mStatus = 0;
    private long mTime = new Date().getTime();

    public WorkerLocation() {
    }

    public WorkerLocation(Location lastLocation, boolean gpsEnabled) {
        int i = 1;
        if (!gpsEnabled) {
            i = 0;
        }
        setGpsStatus(i);
        setLatitude(lastLocation.getLatitude());
        setLogitude(lastLocation.getLongitude());
        setSpeed((lastLocation.getSpeed() / 1000.0f) * 3600.0f);
        setDirection((double) lastLocation.getBearing());
    }

    public int getProtocolVersion() {
        return this.mProtocolVersion;
    }

    public void setProtocolVersion(int mProtocolVersion) {
        this.mProtocolVersion = mProtocolVersion;
    }

    public long getBlockNumber() {
        return this.mBlockNum;
    }

    public void setBlockNumber(long mBlockNum) {
        this.mBlockNum = mBlockNum;
    }

    public int getStatus() {
        return this.mStatus;
    }

    public void setStatus(int mStatus) {
        this.mStatus = mStatus;
    }

    public int getGpsStatus() {
        return this.mGpsStatus;
    }

    public void setGpsStatus(int mGpsStatus) {
        this.mGpsStatus = mGpsStatus;
    }

    public long getTime() {
        return this.mTime;
    }

    public void setTime(long mTime) {
        this.mTime = mTime;
    }

    public long getTimeInSeconds() {
        return this.mTime / 1000;
    }

    public String getTimeStr() {
        return Long.toString(this.mTime);
    }

    public double getLatitude() {
        return this.mLatitude;
    }

    public void setLatitude(double mLatitude) {
        this.mLatitude = mLatitude;
    }

    public double getLongitude() {
        return this.mLongitude;
    }

    public void setLogitude(double mLogitude) {
        this.mLongitude = mLogitude;
    }

    public float getDepth() {
        return this.mDepth;
    }

    public void setDepth(float mDepth) {
        this.mDepth = mDepth;
    }

    public String getDepthStr() {
        return Float.toString(this.mDepth);
    }

    public float getSpeed() {
        return this.mSpeed;
    }

    public void setSpeed(float mSpeed) {
        this.mSpeed = mSpeed;
    }

    public String getSpeedStr() {
        return Float.toString(this.mSpeed);
    }

    public double getDirection() {
        return this.mDirection;
    }

    public void setDirection(double mDirection) {
        this.mDirection = mDirection;
    }

    public String getDirectionStr() {
        return Double.toString(this.mDirection);
    }

    public int getCRC(String in) {
        int i = 0;
        int s = 0;
        char[] buffer = new char[in.length()];
        in.getChars(0, in.length(), buffer, 0);
        while (i < buffer.length) {
            s ^= Character.getNumericValue(buffer[i]);
            i++;
        }
        return s;
    }

    public String getCRCStr(String in) {
        return Long.toString((long) getCRC(in));
    }

    public String toString() {
        String result = "$" + this.mProtocolVersion + "," + this.mBlockNum + "," + this.mStatus + "," +
            this.mGpsStatus + "," + getTimeInSeconds() + "," + "E" + this.mLongitude + "," + "N" +
            this.mLatitude + "," + this.mDepth + "," + this.mSpeed + "," + this.mDirection + "*";
        return result + getCRCStr(result) + "!";
    }
}
