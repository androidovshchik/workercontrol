package com.workercontrol;

import java.io.IOException;

public interface ITcpClient {

    String BEGIN_TAG = "$";
    String DELIMITTER = ",";
    String END_TAG = "!";
    int MAX_PORT = 9999;
    int MIN_PORT = 0;
    String STOP_TAG = "*";

    void closeConnection() throws IOException;

    String getAnswer() throws IOException;

    String getHost();

    int getPort();

    boolean isConnected();

    boolean openConnection(String str, int i);

    void sendData(String data) throws IOException;

    void sleep(long j);
}
