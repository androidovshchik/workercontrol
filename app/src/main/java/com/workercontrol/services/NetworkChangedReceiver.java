package com.workercontrol.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.workercontrol.MainActivity;
import com.workercontrol.components.FileManager;
import com.workercontrol.components.PreferencesManager;
import com.workercontrol.models.Preferences;

public class NetworkChangedReceiver extends BroadcastReceiver {

    @Override
    @SuppressWarnings("all")
    public void onReceive(Context context, Intent intent) {
        NetworkInfo netInfo = ((ConnectivityManager) context.getSystemService("connectivity"))
            .getActiveNetworkInfo();
        SharedPreferences mSharedPreferences = context.getSharedPreferences(MainActivity.PREF_NAME, 0);
        Preferences mClientPreferences = PreferencesManager.readPreferences(mSharedPreferences);
        if (!mClientPreferences.isTracking()) {
            return;
        }
        if (netInfo == null || !netInfo.isConnectedOrConnecting()) {
            FileManager.debug(context, NetworkChangedReceiver.class, "connectionLost");
        }
    }
}
