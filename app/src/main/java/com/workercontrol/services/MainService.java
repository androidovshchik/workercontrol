package com.workercontrol.services;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Build;
import android.os.PowerManager;
import android.util.Log;

import com.squareup.sqlbrite3.BriteDatabase;
import com.workercontrol.BuildConfig;
import com.workercontrol.MainActivity;
import com.workercontrol.R;
import com.workercontrol.components.FileManager;
import com.workercontrol.components.LocationControl;
import com.workercontrol.components.Utils;
import com.workercontrol.models.Backup;
import com.workercontrol.models.Row;
import com.workercontrol.models.Subscriber;
import com.workercontrol.models.WorkerLocation;

import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class MainService extends BaseService {

    public static final int START_SERVICE_NOTIFICATION_ID = 5002;
    private static final String TAG = "MainService";

    private LocationControl mLocationControl;
    private NetworkChangedReceiver networkChangedReceiver;
    private PowerManager.WakeLock wakeLock;

    @Override
    @SuppressWarnings("all")
    public void onCreate() {
        super.onCreate();
        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
            getString(R.string.app_name));
        wakeLock.acquire();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager notificationManager = (NotificationManager)
                getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel notificationChannel = new NotificationChannel(Utils.NOTIFICATION_DEFAULT_CHANNEL_ID,
                getString(R.string.app_name), NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        startForeground(START_SERVICE_NOTIFICATION_ID, Utils.makeNotification(getApplicationContext(),
            getString(R.string.notification_message), R.drawable.ic_gps_fixed_white_24dp,
            PendingIntent.getActivity(getApplicationContext(), 0, new Intent(getApplicationContext(),
                MainActivity.class), 0), true));
        mLocationControl = new LocationControl(getApplicationContext());
        networkChangedReceiver = new NetworkChangedReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(networkChangedReceiver, intentFilter);
        FileManager.debug(getApplicationContext(), MainService.class, "onCreate",
            mLocationControl.mActiveProviders.toString());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        FileManager.debug(getApplicationContext(), MainService.class, "onStartCommand");
        disposable.add(Observable.fromCallable(new Callable<Boolean>() {

            @Override
            public Boolean call() {
                WorkerLocation wLocation;
                Location lastLocation = mLocationControl.getLastLocation();
                if (lastLocation != null) {
                    wLocation = new WorkerLocation(lastLocation, mLocationControl.isGpsEnabled());
                } else {
                    wLocation = new WorkerLocation();
                }
                wLocation.setStatus(1);
                wLocation.setBlockNumber(mClientPreferences.getUserName());
                wLocation.setTime(new Date().getTime());
                boolean hasSent = sendWorkerLocation(wLocation.toString());
                if (!hasSent) {
                    manager.insertRow(new Backup(wLocation));
                }
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, wLocation.toString());
                }
                FileManager.debug(getApplicationContext(), MainService.class, wLocation.toString(),
                    "hasSent: " + hasSent);
                return hasSent;
            }
        }).subscribeOn(Schedulers.io())
            .subscribeWith(new Subscriber<Boolean>() {

                @Override
                public void onNext(Boolean hasSent) {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "hasSent current: " + hasSent);
                    }
                    if (hasSent) {
                        checkBackup();
                    } else {
                        stopWork();
                    }
                }

                @Override
                public void onError(Throwable e) {
                    FileManager.exception(getApplicationContext(), MainService.class, e.toString());
                    stopWork();
                }
            }));
        return START_NOT_STICKY;
    }

    private void checkBackup() {
        disposable.add(Observable.fromCallable(new Callable<ArrayList<Backup>>() {

            @Override
            public ArrayList<Backup> call() {
                ArrayList<Backup> backup = new ArrayList<>();
                BriteDatabase.Transaction transaction = manager.db.newTransaction();
                try {
                    manager.db.execute("DELETE FROM backup WHERE sent = 1");
                    backup.addAll(Row.getRows(manager.db.query("SELECT rowid, * FROM backup" +
                        " WHERE sent = 0 LIMIT 10"), Backup.class));
                    transaction.markSuccessful();
                } catch (Exception e) {
                    FileManager.exception(getApplicationContext(), MainService.class, e.toString());
                    backup.clear();
                } finally {
                    transaction.end();
                }
                return backup;
            }
        }).subscribeOn(Schedulers.io())
            .subscribeWith(new Subscriber<ArrayList<Backup>>() {

                @Override
                public void onNext(ArrayList<Backup> backup) {
                    processBackup(backup);
                }

                @Override
                public void onError(Throwable e) {
                    FileManager.exception(getApplicationContext(), MainService.class, e.toString());
                    stopWork();
                }
            }));
    }

    private void processBackup(ArrayList<Backup> backup) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "backup size: " + backup.size());
        }
        if (backup.isEmpty()) {
            stopWork();
            return;
        }
        Backup item = backup.get(0);
        backup.remove(0);
        boolean hasSent = sendWorkerLocation(item.data);
        FileManager.debug(getApplicationContext(), MainService.class, "backup size: " +
            backup.size(), item.toString(), "hasSent: " + hasSent);
        if (hasSent) {
            item.sent = true;
            manager.updateRow(item);
        } else {
            stopWork();
            return;
        }
        processBackup(backup);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        FileManager.debug(getApplicationContext(), MainService.class, "onDestroy");
        if (mLocationControl != null) {
            mLocationControl.onDestroy();
        }
        unregisterReceiver(networkChangedReceiver);
        wakeLock.release();
        stopForeground(true);
    }
}
