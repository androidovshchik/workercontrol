package com.workercontrol.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.workercontrol.ITcpClient;
import com.workercontrol.MainActivity;
import com.workercontrol.components.FileManager;
import com.workercontrol.components.PreferencesManager;
import com.workercontrol.components.TcpClient;
import com.workercontrol.data.DbManager;
import com.workercontrol.models.Preferences;

import java.io.IOException;

import io.reactivex.disposables.CompositeDisposable;

public abstract class BaseService extends Service {

    protected ITcpClient mClient;
    protected Preferences mClientPreferences;
    protected DbManager manager;
    protected CompositeDisposable disposable = new CompositeDisposable();

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mClientPreferences = PreferencesManager.readPreferences(getSharedPreferences(MainActivity.PREF_NAME, 0));
        mClient = new TcpClient();
        manager = new DbManager(getApplicationContext());
    }

    protected boolean sendWorkerLocation(String wLocation) {
        if (!mClient.isConnected() && !mClient.openConnection(mClientPreferences.getHost(),
            mClientPreferences.getPort())) {
            return false;
        }
        try {
            mClient.sendData(wLocation);
            return true;
        } catch (IOException e1) {
            if (mClient.isConnected()) {
                try {
                    mClient.closeConnection();
                } catch (IOException e2) {
                    FileManager.exception(getApplicationContext(), getClass(), e2.toString());
                }
            }
        }
        return false;
    }

    protected void stopWork() {
        disposable.clear();
        stopSelf();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        disposable.dispose();
        if (this.mClient != null && this.mClient.isConnected()) {
            try {
                this.mClient.closeConnection();
            } catch (IOException e) {
                FileManager.exception(getApplicationContext(), getClass(), e.toString());
            }
        }
    }
}
