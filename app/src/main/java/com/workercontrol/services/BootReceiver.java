package com.workercontrol.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.workercontrol.AlarmUtil;
import com.workercontrol.MainActivity;
import com.workercontrol.components.PreferencesManager;
import com.workercontrol.components.Utils;
import com.workercontrol.models.Preferences;

public class BootReceiver extends BroadcastReceiver {

    @Override
    @SuppressWarnings("all")
    public void onReceive(Context context, Intent intent) {
        SharedPreferences mSharedPreferences = context.getSharedPreferences(MainActivity.PREF_NAME, 0);
        Preferences mClientPreferences = PreferencesManager.readPreferences(mSharedPreferences);
        if (mSharedPreferences.getAll().size() > 0 && mClientPreferences.isTracking()) {
            AlarmUtil.next(context, mClientPreferences.getPeriod() * 1000L, BootReceiver.class);
            Utils.startServiceRightWay(context, MainService.class);
        }
    }
}
