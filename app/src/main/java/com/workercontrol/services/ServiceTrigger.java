package com.workercontrol.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.workercontrol.AlarmUtil;
import com.workercontrol.MainActivity;
import com.workercontrol.components.FileManager;
import com.workercontrol.components.PreferencesManager;
import com.workercontrol.components.Utils;
import com.workercontrol.models.Preferences;

public class ServiceTrigger extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		SharedPreferences mSharedPreferences = context.getSharedPreferences(MainActivity.PREF_NAME, 0);
		Preferences mClientPreferences = PreferencesManager.readPreferences(mSharedPreferences);
        FileManager.debug(context, AlarmUtil.class, "ServiceTrigger: received",
            "is tracking " + mClientPreferences.isTracking());
		if (mClientPreferences.isTracking()) {
			AlarmUtil.next(context, mClientPreferences.getPeriod() * 1000L, ServiceTrigger.class);
            Utils.startServiceRightWay(context, MainService.class);
		}
	}
}
